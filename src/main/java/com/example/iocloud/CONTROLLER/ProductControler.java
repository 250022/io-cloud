package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductControler {

    ProductManager manager;

    @Autowired
    public ProductControler(ProductManager manager) {this.manager=manager;
    }

    @GetMapping("/products/all")
    public Iterable<Product> getAll() {
        return manager.findAllBy();
    }

    @GetMapping("/products")
    public Optional<Product> getById(@RequestParam Long id) {
        return manager.findAllById(id);
    }


    @PostMapping("/admin/products")
    public Product addProduct(@RequestBody Product product) {
        return manager.addProduct(product);
    }

    @PutMapping("/admin/products")
    public Product updateProduct(@RequestBody Product product) {
        return manager.addProduct(product);
    }

    @PatchMapping("/admin/products")
    public Product patchProduct(@RequestBody Map<String,Object> map, @RequestParam long id) {
        return manager.update(map,id);
    }



}
