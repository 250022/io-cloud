package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class CustomerControler {


    CustomerManager manager;

    @Autowired
    public CustomerControler(CustomerManager manager) {
        this.manager = manager;
    }


    @GetMapping("/customer/all")
    public Iterable<Customer> getAll() {
        return manager.findAllBy();
    }

    @GetMapping("/customer")
    public Optional<Customer> getById(@RequestParam long id) {
        return manager.findAllById(id);
    }


    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer) {
        return manager.addCustomer(customer);
    }

    @PutMapping("/admin/customer")
    public Customer updateCustomer(@RequestBody Customer customer) {
        return manager.addCustomer(customer);
    }

    @PatchMapping("/admin/customer")
    public Customer patchCustomer(@RequestBody Map<String,Object> map, @RequestParam long id) {
        return manager.update(map,id);
    }


}
