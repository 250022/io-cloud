package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.Product;
import com.example.iocloud.MODEL.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductManager {

    @Autowired
    private ProductRepo productRepo;

    public ProductManager(){}

    public Optional<Product>findAllById(Long id){return productRepo.findById(id);}

    public Iterable<Product>findAllBy(){return productRepo.findAll();}

    public Product addProduct(Product product){return productRepo.save(product);}

    public Product update(Map<String,Object> map, long id){
        Optional<Product> product = productRepo.findById(id);
        if(!product.isPresent())
            return null;

        Product product1 = product.get();
        if(map.containsKey("name")) product1.setName((String) map.get("name"));
        if(map.containsKey("price")) product1.setPrice(Float.parseFloat(map.get("price").toString()) );
        if(map.containsKey("available")) product1.setAvailable((Boolean) map.get("available"));
        addProduct(product1);
        return product1;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill(){

        addProduct(new Product("Długopis", 1.99f,true));
        addProduct(new Product("Piórnik", 10.99f,true));
    }

}
