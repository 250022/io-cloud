package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.*;
import com.example.iocloud.MODEL.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserControler {

    @Autowired
    private UserRepo userRepo;


    public UserControler() {
    }

    public Optional<UserDTO> findAllById(Long id){
        return userRepo.findById(id);
    }

    public Iterable<UserDTO> findAllBy(){
        return userRepo.findAll();
    }

    public UserDTO addUser(UserDTO userDTO){
        return userRepo.save(userDTO);
    }


    public Optional<UserDTO> findByName(String name){
        return userRepo.findByName(name);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill(){

        UserDTOBuilder userDTOBuilder = new UserDTOBuilder();
        User admin = new User("admin","verylongpassword","ROLE_ADMIN");
        User user = new User("user","verylongpassword","ROLE_CUSTOMER");


        addUser(userDTOBuilder.userDTO(admin));
        addUser(userDTOBuilder.userDTO(user));

    }

}
