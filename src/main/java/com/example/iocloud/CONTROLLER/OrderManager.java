package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.Customer;
import com.example.iocloud.MODEL.CustomerOrder;
import com.example.iocloud.MODEL.OrderRepo;
import com.example.iocloud.MODEL.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Service
public class OrderManager {
    @Autowired
    private OrderRepo orderRepo;

    public OrderManager() {
    }

    public Optional<CustomerOrder> findAllById(Long id){
        return orderRepo.findById(id);
    }

    public Iterable<CustomerOrder> findAllBy(){
        return orderRepo.findAll();
    }
    
    public CustomerOrder addCustomerOrder(CustomerOrder order){
        return orderRepo.save(order);
    }

    public CustomerOrder update(Map<String,Object> map, long id){
        Optional<CustomerOrder> order = orderRepo.findById(id);
        if(!order.isPresent())
            return null;

        CustomerOrder customerOrder = order.get();
        if(map.containsKey("status")) customerOrder.setStatus((String) map.get("status"));
        if(map.containsKey("placeDate")) customerOrder.setPlaceDate((LocalDateTime) map.get("placeDate"));
        if(map.containsKey("products")) customerOrder.setProducts((Set<Product>) map.get("products"));
        if(map.containsKey("customer")) customerOrder.setCustomer((Customer) map.get("customer"));

        addCustomerOrder(customerOrder);
        return customerOrder;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill(){
        Product product = new Product("Długopis",2.99f,true);
        Product product1 = new Product("Ołówek", 1.99f, true);
        Customer customer = new Customer("Anna Kowalska","Leszno");
        Customer customer2 = new Customer("Agata Nowak","Wrocław");
        Set<Product> products = new HashSet<>(){
            {
                add(product);
                add(product1);
            }
        };
        addCustomerOrder(new CustomerOrder(customer,products, LocalDateTime.now(),"Wysłane" ));
        addCustomerOrder(new CustomerOrder(customer2,products, LocalDateTime.now(),"Opłacone" ));

    }



}
