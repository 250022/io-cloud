package com.example.iocloud.CONTROLLER;

import com.example.iocloud.MODEL.Customer;
import com.example.iocloud.MODEL.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;


@Service
public class CustomerManager {
    @Autowired
    private CustomerRepo customerRepo;


    public CustomerManager() {

    }


    public Optional<Customer> findAllById(Long id){
        return customerRepo.findById(id);
    }

    public Iterable<Customer> findAllBy(){
        return customerRepo.findAll();
    }

    public Customer addCustomer(Customer customer){
        return customerRepo.save(customer);
    }

    public Customer update(Map<String,Object> map, long id){
        Optional<Customer> customer = customerRepo.findById(id);
        if(!customer.isPresent())
            return null;

        Customer customer1= customer.get();
        if(map.containsKey("name")) customer1.setName((String) map.get("name"));
        if(map.containsKey("address")) customer1.setAddress((String) map.get("address"));

        addCustomer(customer1);
        return customer1;
    }


    @EventListener(ApplicationReadyEvent.class)
    public void fill(){
        addCustomer(new Customer(1L,"Piotr Góral","Wrocław"));
        addCustomer(new Customer(2L,"Maria Frączewska","Warszawa"));

    }





}
