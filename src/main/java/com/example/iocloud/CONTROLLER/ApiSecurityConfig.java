package com.example.iocloud.CONTROLLER;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    DataSource dataSource;

     @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/h2/**");

    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/products/**").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers("/api/order/**").hasAnyRole("CUSTOMER","ADMIN")
                .antMatchers("/api/customer/**").hasRole("CUSTOMER")
                .antMatchers("/api/admin/**").hasRole("ADMIN")
                .and()
                .formLogin().permitAll().and().logout().permitAll().and()
                .httpBasic().and().csrf().disable().headers().frameOptions().disable();

    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM userdto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM userdto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

}
