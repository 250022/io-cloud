package com.example.iocloud.CONTROLLER;


import com.example.iocloud.MODEL.CustomerOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderControler {


    OrderManager manager;

    @Autowired
    public OrderControler(OrderManager manager) {
        this.manager = manager;
    }

    @GetMapping("/order/all")
    public Iterable<CustomerOrder> getAll() {
        return manager.findAllBy();
    }

    @GetMapping("/order")
    public Optional<CustomerOrder> getById(@RequestParam long id) {
        return manager.findAllById(id);
    }

    @PostMapping("/admin/order")
    public CustomerOrder addOrder(@RequestBody CustomerOrder order) {
        return manager.addCustomerOrder(order);
    }

    @PutMapping("/admin/order")
    public CustomerOrder updateOrder(@RequestBody CustomerOrder order) {
        return manager.addCustomerOrder(order);
    }

    @PatchMapping("/admin/order")
    public CustomerOrder patchOrder(@RequestBody Map<String, Object> map, @RequestParam long id) {
        return manager.update(map, id);
    }


}
