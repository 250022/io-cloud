package com.example.iocloud.MODEL;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo extends CrudRepository<CustomerOrder,Long> {

}
