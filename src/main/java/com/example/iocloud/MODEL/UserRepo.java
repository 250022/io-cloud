package com.example.iocloud.MODEL;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends CrudRepository<UserDTO,Long> {
    Optional<UserDTO> findByName(String name);
}
