package com.example.iocloud.MODEL;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Product {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private long id;
    private String name;
    private float price;
    private boolean available;

    @ManyToMany
    private Set<CustomerOrder>orderedProducts = new HashSet<>();


    public Product( String name, float price, boolean available) {
        this.name = name;
        this.price = price;
        this.available = available;
    }


    public Product() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Set<CustomerOrder> getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(Set<CustomerOrder> orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
