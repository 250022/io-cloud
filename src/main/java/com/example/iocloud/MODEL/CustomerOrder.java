package com.example.iocloud.MODEL;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
public class CustomerOrder {


        @Id
        @GeneratedValue(strategy =  GenerationType.IDENTITY)
        private long id;
        private String status;
        private LocalDateTime placeDate;

        @ManyToMany(mappedBy = "orderedProducts", cascade = CascadeType.MERGE)
        private Set<Product> products = new HashSet<>();

        @ManyToOne(cascade = CascadeType.ALL)
        private Customer customer;



        public CustomerOrder(Customer customer, Set<Product> products, LocalDateTime placeDate, String status) {
            this.customer = customer;
            this.products = products;
            this.placeDate = placeDate;
            this.status = status;
        }

        public CustomerOrder() {
        }


        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public Set<Product> getProducts() {
            return products;
        }

        public void setProducts(Set<Product> products) {
            this.products = products;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public LocalDateTime getPlaceDate() {
            return placeDate;
        }

        public void setPlaceDate(LocalDateTime placeDate) {
            this.placeDate = placeDate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    

}
